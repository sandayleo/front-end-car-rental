import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from 'axios'
import  './permissions'
import * as echarts from 'echarts';

Vue.prototype.$echarts=echarts;
Vue.config.productionTip = false

//让Vue引入使用ElementUI
Vue.use(ElementUI);
//设置路径的默认前缀
axios.defaults.baseURL="http://localhost:14966"
//使用axios拦截器，为所有请求加上token
axios.interceptors.request.use(function(config){
  var token  = localStorage.getItem("token");
	//比如是否需要设置 token
  if(token){
   // alert(config.url);
    var url = config.url;
    //alert(url);  "123123123".indexOf("?")=-1   "123?123123".indexOf("?")=3
    if(url.indexOf("?")!=-1){
      config.url=url+"&token="+token;
    }else{
      config.url=url+"?token="+token;
    }
    //alert(config.url);
    // alert(JSON.stringify(config));
    //config.headers.token=sessionStorage.getItem("token");
  }
  return config;
})

//把axios挂载到vue对象
Vue.prototype.$http=axios;

//路由之前拦截处理
router.beforeEach((to, from, next) => {
  //获取请求路径
  const path = to.path;
  //放行路径
    if (path === "/Login") {
      //next 让程序继续运行
        return next();
    }
    //获取token值
  const token = localStorage.getItem("token");
  // token不空，放行
  if (token) {
    //alert(next);
    //alert(to);
   // alert(111);
    next()
  } else {
    //为空 到登录页面
    next({ path: '/Login' })
  }
})

new Vue({
  router,
  store,
  // created(){
  //   this.$router.push('/Login');
  // },
  render: h => h(App)
}).$mount('#app')
